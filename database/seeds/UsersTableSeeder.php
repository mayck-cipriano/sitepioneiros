<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("users")->insert([
            "name" => "mayck.cipriano",
            "email" => "mayck.cipra@gmail.com",
            "password" => bcrypt("Colina@1965"),
            "created_at" => "2018-02-21 01:36:50",
            "updated_at" => "2018-02-21 01:36:50"
        ]);
    }
}
