<?php

use Illuminate\Database\Seeder;

class ContentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table()->insert(
            array(
                array(
                    "page_id" => 1,
                    "title" => "PRIMEIRO DEUS",
                    "subtitle" => "FAMÍLIA: Presente de Deus",
                    "imagem_id" => 4,
                    "order" => 3,
                    "goto" => "Assista",
                    "goto_url" => "/10DiasDeOracao/FamiliaPresenteDeDeus",
                    "created_at" => "2018-02-22 03:05:06"
                ),
                array(
                    "page_id" => 1,
                    "title" => "PRIMEIRO DEUS",
                    "subtitle" => "PERDÃO: Restaurador de Pontes",
                    "imagem_id" => 5,
                    "order" => 2,
                    "goto" => "Assista",
                    "goto_url" => "/10DiasDeOracao/PerdaoRestauradorDePontes",
                    "created_at" => "2018-02-23 08:27:09"
                ),
                array(
                    "page_id" => 1,
                    "title" => "PRIMEIRO DEUS",
                    "subtitle" => "Vitória Sobre a Tentação",
                    "imagem_id" => 6,
                    "order" => 1,
                    "goto" => "Assista",
                    "goto_url" => "/10DiasDeOracao/VitoriaSobreATentacao",
                    "created_at" => \Carbon\Carbon::now()->format( "YYYY-MM-DD HH:ii:ss" )
                )
            )
        );
        /*
        DB::table("contents")->insert(
            array(,
                array(
                    "page_id" => 1,
                    "title" => "AMAR",
                    "subtitle" => "Vivemos o amor de Cristo e o espalhamos ao mundo", "imagem_id" => 2, "order" => 1,
                    "created_at" => "2018-02-13 20:05:06"
               ),
               array(
                    "page_id" => 1,
                    "title" => "SALVAR",
                    "subtitle" => "Mostramos Cristo como salvador para livrar das algemas do pecado.", "imagem_id" => 1, "order" => 2,
                    "created_at" => "2018-02-13 20:21:25"
               ),
               array(
                   "page_id" => 1,
                    "title" => "SERVIR",
                    "subtitle" => "Ensinamos a como compartilhar o viver com Cristo.",
                   "imagem_id" => 3,
                   "order" => 3,
                    "created_at" => "2018-02-13 20:22:15"
                )
            )
        );

        DB::table("contents")->insert(
            array(
                array(
                    "page_id" => 2,
                    "title" => "Ao redor do mundo",
                    "subtitle" => "Uma pequena idéia, iniciada em paralelo ao redor dos Estados Unidos, muda o relacionamento de juvenis e adolescentes com a natureza e o seu Criador..",
                    "icon" => "fa fa-globe",
                    "order" => 1,
                    "created_at" => "2018-02-13 21:56:47"
                ),
                array(
                    "page_id" => 2,
                    "title" => "AO CHEGAR NA COLINA",
                    "subtitle" => "Fábrica de teologandos, a colina é conhecida por centro de formação de pastores para levar à mensagem da breve volta de Jesus. Um destes, aceita o chamado de evangelhizar os mais novos.",  "icon" => "fa fa-history", "order" => 2,
                    "created_at" => "2018-02-13 21:57:57"
                ),
                array(
                    "page_id" => 2,
                    "title" => "Nos dias Atuais",
                    "subtitle" => "Nessa seção você saberá mais sobre a nossa agenda, comunicados, documentos úteis e outras informações atuais.",  "icon" => "fa fa-calendar", "order" => 3,
                    "created_at" => "2018-02-13 21:58:41"
                )
            )
        );
        */
    }
}
