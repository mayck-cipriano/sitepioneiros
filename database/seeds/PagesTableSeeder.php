<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("pages")->insert(
            array(
                array( "menu_id" => "1", "title" => "Header Home", "subtitle" => "Carousel da home", "created_at" => "2018-02-13 20:03:01" )/*,
                array( "menu_id" => "2", "title" => "Sobre Nós", "subtitle" => "Nossa história, seja como Desbravador ou como Pioneiros da Colina, é incrível e cheia de providência divina. Aproveite para conhecer um pouco sobre nós, do passado até hoje.", "created_at" => "2018-02-13 21:55:27" ) */
            )
        );
    }
}
