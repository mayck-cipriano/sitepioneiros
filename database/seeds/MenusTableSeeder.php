<?php

use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menus')->insert(
            array(
                array("descricao" => "Home","key" => "home", "url" => "/", "view" => "partials.carousel-large","is_show" => "1", "is_authenticate" => "0", "ordem" => "1", "created_at" => "2018-02-13 06:58:39"),
                array("descricao" => "Sobre Nós","key" => "about-us", "url" => "/#about-us", "view" => "partials.columns","is_show" => "1", "is_authenticate" => "0", "ordem" => "2", "created_at" => "2018-02-13 06:58:39"),
                array("descricao" => "Unidades","key" => "unidades", "url" => "/#unidades", "view" => "partials.unidades","is_show" => "1", "is_authenticate" => "0", "ordem" => "3", "created_at" => "2018-02-13 14:58:35"),
                array("descricao" => "Galeria","key" => "galeria", "url" => "/#galeria", "view" => "partials.cards","is_show" => "1", "is_authenticate" => "0", "ordem" => "4", "created_at" => "2020-02-13 16:40:23"),
                array("descricao" => "Diretoria","key" => "diretoria", "url" => "/#diretoria", "view" => "partials.team","is_show" => "1", "is_authenticate" => "0", "ordem" => "5", "created_at" => "2019-02-13 16:45:49"),
                array("descricao" => "Depoimentos","key" => "depoimentos", "url" => "/#depoimentos", "view" => "partials.depoimentos","is_show" => "1", "is_authenticate" => "0", "ordem" => "6", "created_at" => "2019-02-13 17:03:01"),
                array("descricao" => "Eventos","key" => "eventos", "url" => "/#eventos", "view" => "partials.blog","is_show" => "1", "is_authenticate" => "0", "ordem" => "7", "created_at" => "2019-02-13 17:11:48"),
                array("descricao" => "Contato","key" => "contato", "url" => "/#contato", "view" => "partials.contact","is_show" => "1", "is_authenticate" => "0", "ordem" => "8", "created_at" => "2018-02-13 17:20:25")
            )
        );

    }
}

/*
 *

 */