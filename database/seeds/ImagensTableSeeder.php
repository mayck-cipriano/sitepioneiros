<?php

use Illuminate\Database\Seeder;

class ImagensTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('imagens')->insert(
            array(
                array( "path" => "images/fotos/batismo0.jpg", "created_at" => "2018-02-13 20:49:37" ),
                array(  "path" => "images/fotos/lenco.jpg", "created_at" => "2018-02-13 20:49:57" ),
                array(  "path" => "images/fotos/diretoria2017.jpg", "created_at" => "2018-02-13 20:50:27"),
                array(  "path" => "images/10dias/dia1.jpg", "created_at" => "2018-02-13 20:50:27"),
                array(  "path" => "images/10dias/dia2.jpg", "created_at" => "2018-02-13 20:50:27"),
                array(  "path" => "images/10dias/dia3.jpg", "created_at" => \Carbon\Carbon::now()->format("YYYY-mm-dd HH:ii:ss"))
            )
        );
    }
}
