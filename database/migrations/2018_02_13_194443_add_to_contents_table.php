<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddToContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Contents', function (Blueprint $table) {
            $table->unsignedInteger( "imagem_id" )->nullable()->after("descricao");
            $table->string("goto")->nullable()->after("descricao");
            $table->string("goto_url")->nullable()->after("goto");
            $table
                ->foreign( "imagem_id" )
                ->references( "id" )
                ->on( "imagens")
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Contents', function (Blueprint $table) {
            $table->dropForeign("contents_imagem_id_foreign");
            $table->dropColumn( "imagem_id");
            $table->dropColumn("goto");
            $table->dropColumn("goto_url");
        });
    }
}
