<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');
            $table->string("descricao", 30);
            $table->string("key", 30);
            $table->string("url", 30)->nullable();
            $table->string("view", 30)->nullable();
            $table->boolean("is_show");
            $table->boolean('is_authenticate');
            $table->unsignedInteger("parent_id")->nullable();
            $table->unsignedInteger("ordem");

            $table->timestamps();
            $table->softDeletes();

            $table->foreign("parent_id")->references("id")->on("menus");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
