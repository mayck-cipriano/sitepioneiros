<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Contents', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger( 'page_id');
            $table->string("title", 50)->nullable();
            $table->string("subtitle", 250)->nullable();
            $table->multiLineString("descricao");
            $table->unsignedTinyInteger("order" );
            $table->timestamps();
            $table->softDeletes();

            // FKs
            $table->foreign("page_id")->references("id")->on("pages");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Contents');
    }
}
