<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Pages', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger( 'menu_id');
            $table->string("title", 50)->nullable();
            $table->string("subtitle", 250)->nullable();
            $table->timestamps();
            $table->softDeletes();

            // FKs
            $table->foreign("menu_id")->references("id")->on("menus");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Pages');
    }
}
