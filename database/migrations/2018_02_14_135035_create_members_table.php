<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger( "user_id");
            $table->unsignedInteger( "sgc_id" );
            $table->string( "name", 150 );
            $table->date( "birth" );
            $table->enum( "gender", array( 'F', 'M' ) );
            $table->date( "baptism" );            $table->string( "email", 250 )->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign( "user_id" )->references( "id" )->on( "users" );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
