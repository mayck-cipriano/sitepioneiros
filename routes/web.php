<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index' );

// Projeto 10 dias de Oração
Route::get('/10DiasDeOracao', 'DezDiasDeOracao2018Controller@index' );

// TODO Refazer rotas abaixo.
Route::get('/10DiasDeOracao/FamiliaPresenteDeDeus', 'DezDiasDeOracao2018Controller@familia' );
Route::get('/10DiasDeOracao/PerdaoRestauradorDePontes', 'DezDiasDeOracao2018Controller@perdao' );
Route::get('/10DiasDeOracao/VitoriaSobreATentacao', 'DezDiasDeOracao2018Controller@vitoria' );
Route::get('/10DiasDeOracao/IntimidadeComDeusNossaMaior', 'DezDiasDeOracao2018Controller@intimidade' );
Route::get('/10DiasDeOracao/ConsoloEEsperancaEmDeus', 'DezDiasDeOracao2018Controller@consolo' );
Route::get('/10DiasDeOracao/NaoDesistaDeSuaFamilia', 'DezDiasDeOracao2018Controller@naoDesista' );
Route::get('/10DiasDeOracao/DoLarParaMissao', 'DezDiasDeOracao2018Controller@missao' );
Route::get('/10DiasDeOracao/PoderDoEspiritoSanto', 'DezDiasDeOracao2018Controller@espiritoSanto' );
Route::get('/10DiasDeOracao/FidelidadeDiaria', 'DezDiasDeOracao2018Controller@fidelidadeDiaria' );
Route::get('/10DiasDeOracao/VitoriaFinal', 'DezDiasDeOracao2018Controller@vitoriaFinal' );

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/comunicado', 'WelcomeController@comunicado');
Route::get('/secretaria/{subsecao?}/{page?}', 'SecretariaController@index');
Route::get('/tesouraria/{subsecao?}/{page?}', 'TesourariaController@index');
Route::get('/dsa/{subsecao?}/{page?}', 'TesourariaController@index');
Route::get('/download/{filename}', 'WelcomeController@download');
Route::get('/calendario', 'WelcomeController@calendario');
