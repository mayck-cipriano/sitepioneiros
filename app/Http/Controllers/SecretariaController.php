<?php
/**
 * Created by PhpStorm.
 * User: vo
 * Date: 13/02/2018
 * Time: 06:25
 */

namespace App\Http\Controllers;


class SecretariaController extends PioneirosController
{
    public function index( $subsecao = null, $page = null ){
        //return response()->json(array( "key" => $this->getKey( "secretaria", $subsecao, $page ) ) );
        return view('default', $this->getDataSourceFromKey( array( "secretaria", $subsecao, $page ) ) ); // Menu::all()
    }

    public function submenu( $submenu ){
        return view('default', $this->getDataSourceFromKey( $submenu ) ); // Menu::all()
    }

    private function contents(){
        $dataSource = array(
            "key" => "secretaria",
            "view" => "partials.columns",
            "path" => array(
                array( "url" => "/", "label" => "Home"),
                array( "label" => "Secretaria")
            ),
            "title" => "SECRETARIA",
            "subtitle" => "Encontre aqui todo o material de secretaria: comunicados, pautas de reunião, calendário, entre outros. Consulte sempre essa página, pois iremos atualizá-la constantemente.",
            "contents" => array(
                array(
                    "title" => "Comunicados",
                    "subtitle" => "Veja aqui todos os comunicados do clube, emitidos ao longo do ano.",
                    "goto" => "Acesse",
                    "goto_url" => "/secretaria/comunicados",
                    "image_url" => ""
                ),
                array(
                    "title" => "Pautas",
                    "subtitle" => "Veja aqui todas as pautas de reunião de pais.",
                    "goto" => "Acesse",
                    "goto_url" => "/secretaria/pautas",
                    "image_url" => ""
                ),
                array(
                    "title" => "Autorizações",
                    "subtitle" => "Pegue aqui as autorizações de saída para os nossos eventos.",
                    "goto" => "Acesse",
                    "goto_url" => "/secretaria/comunicados",
                    "image_url" => ""
                ),
                array(
                    "title" => "Calendário",
                    "subtitle" => "Consulte os eventos que teremos ao longo do ano..",
                    "goto" => "Acesse",
                    "goto_url" => "/secretaria/calendario",
                    "image_url" => ""
                )
            )
        );
        return $dataSource;
    }
}