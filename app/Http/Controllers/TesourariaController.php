<?php
/**
 * Created by PhpStorm.
 * User: vo
 * Date: 13/02/2018
 * Time: 06:25
 */

namespace App\Http\Controllers;


class TesourariaController extends PioneirosController
{
    public function index( $subsecao = null, $page = null ){
        //return response()->json(array( "key" => $this->getKey( "secretaria", $subsecao, $page ) ) );
        return view('default', $this->getDataSourceFromKey( array( "home", $subsecao, $page ) ) ); // Menu::all()
    }

    public function submenu( $submenu ){
        return view('default', $this->getDataSourceFromKey( $submenu ) ); // Menu::all()
    }
}