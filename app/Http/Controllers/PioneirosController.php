<?php
/**
 * Created by PhpStorm.
 * User: vo
 * Date: 28/03/2018
 * Time: 01:21
 */

namespace App\Http\Controllers;

use App\Contents;
use App\Imagem;
use App\Menu;
use App\Pages;
use Illuminate\Routing\Controller as BaseController;

class PioneirosController extends BaseController
{
    protected function getParameters( $retorno = array() ){
        $menus = Menu::all();
        $arvoreMenu = array();
        $arvoreView = array();
        $dataSources = array();

        foreach ( $menus as $menu ){
            if( !$menu->is_show){
                continue;
            }
            array_push( $arvoreMenu, array( "url" => substr( $menu->url, 1 ), "descricao" => $menu->descricao, "view" => $menu->view ) );
            if( $menu->view != null ){
                array_push( $arvoreView, array( "view" => $menu->view ) );
            }
            $page = Pages::where( "menu_id", "=", $menu->id )->first();

            $dataSources[ $menu->view ] = $this->getDataSource( $page, $menu->key );
        }

        return array_merge( $retorno, array( "menu" => $arvoreMenu, "views" => $arvoreView, "dataSources" => $dataSources ) );
    }

    protected function getKey( $keys ){
        $returnKey = null;
        foreach ($keys as $k){
            if( $k == null ) break;
            $returnKey = $k;
        }

        return $returnKey;
    }

    protected function getDataSource( $page, $key ){
        if( $page == null ){
            return array( "key" => $key );
        }
        $retorno = array( "key" => $key, "title" => $page->title, "subtitle" => $page->subtitle, "contents" => $this->getContents( $page ) );

        return $retorno;
    }

    private function findMenuFromKeys( $keys ){
        foreach ( array_reverse( $keys ) as $key ){
            $menu = Menu::where([ "key" => $key ])->first();
            if( isset( $menu ) ) return $menu;
        }
    }

    protected function getDataSourceFromKey( $keys ){
        $key = $this->getKey( $keys );

        if( !isset( $key ) || empty( $key ) || is_array($key) ){
            return response( "", 404 );
        }

        $menu = Menu::where([ "key" => $key ])->first();
        $page = null;
        if( !isset( $menu ) ){
            $page = Pages::where( "keypage", $key )->first();
        }else{
            $page = Pages::where( "menu_id", "=", $menu->id )->first();
        }

        $dataSource = $this->getDataSource(  $page, $key );

        if( !isset( $menu ) ){
            $menu = $this->findMenuFromKeys( $keys );
            $dataSource[ "path" ] = array_reverse( $this->getPath( $menu, true, $page ) );
        }else{
            $dataSource[ "path" ] = array_reverse( $this->getPath( $menu, true ) );
        }

        $dataSource[ "view" ] = $page->view;
        return $this->getParameters( $dataSource );
    }

    protected function getPath( $menu, $isFirst = false, $page = null ){
        if( isset( $page ) ){
            $path = array( array( "label" => $page->title ) );
            return array_merge( $path, $this->getPath( $menu ) );
        }

        $path = array( array( "label" => $menu->descricao, "url" => $isFirst ? "" : $menu->url ) ) ;

        if( isset( $menu->parent_id ) && $menu->parent_id > 0 ){
            return array_merge( $path, $this->getPath( Menu::find( $menu->parent_id ) ) );
        }else{
            return $path;
        }
    }

    protected function getContents( $page ){
        $contents = array();

        foreach( Contents::where( "page_id", "=", $page->id )->orderBy('order', 'asc')->cursor() as $content ){
            $content = $content->toArray();

            if( !empty( $content["imagem_id"] ) ){
                $imagem = Imagem::find( $content["imagem_id"] );
                $content["url_imagem"] = $imagem->path;
            }

            array_push( $contents, $content );
        }

        return $contents;
    }
}