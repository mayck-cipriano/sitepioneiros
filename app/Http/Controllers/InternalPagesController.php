<?php
/**
 * Created by PhpStorm.
 * User: vo
 * Date: 13/02/2018
 * Time: 06:25
 */

namespace App\Http\Controllers;

use App\Contents;
use App\Imagem;
use App\Menu;
use App\Pages;
use Illuminate\Routing\Controller as BaseController;


class InternalPagesController extends BaseController
{


    public function index(){
        return view('page', $this->getParameters() ); // Menu::all()
    }

    private function getParameters(){
        $menus = Menu::all();
        $arvoreMenu = array();
        $arvoreView = array();
        $dataSources = array();

        foreach ( $menus as $item ){
            array_push( $arvoreMenu, array( "url" => substr( $item->url, 1 ), "descricao" => $item->descricao, "view" => $item->view ) );
            array_push( $arvoreView, array( "view" => $item->view ) );
            $dataSources[ $item->view ] = $this->getDataSource( $item );
        }

        return array( "menu" => $arvoreMenu, "views" => $arvoreView, "dataSources" => $dataSources );
    }

    private function getDataSource( $menu ){
        $page = Pages::where( "menu_id", "=", $menu->id )->first();
        if( $page == null ){
            return array( "key" => $menu->key );
        }
        $contents = array();

        foreach( Contents::where( "page_id", "=", $page->id )->cursor() as $content ){
            $content = $content->toArray();

            if( !empty( $content["imagem_id"] ) ){
                $imagem = Imagem::find( $content["imagem_id"] );
                $content["url_imagem"] = $imagem->path;
            }

            array_push( $contents, $content );
        }

        return array( "key" => $menu->key, "title" => $page->title, "subtitle" => $page->subtitle, "contents" => $contents );
    }
}