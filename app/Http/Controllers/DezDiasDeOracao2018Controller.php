<?php
/**
 * Created by PhpStorm.
 * User: vo
 * Date: 13/02/2018
 * Time: 06:25
 */

namespace App\Http\Controllers;

use App\Contents;
use App\Imagem;
use App\Menu;
use App\Pages;
use Carbon\Carbon;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\App;


class DezDiasDeOracao2018Controller extends BaseController
{
    public function index(){
        return view('page-list', $this->getParameters( $this->getLista() ) );
    }

    public function familia(){
        return view('page', $this->getParameters( $this->getDia( 1 ) ) );
    }

    public function perdao(){
        return view('page', $this->getParameters( $this->getDia( 2 ) ) );
    }

    public function vitoria(){
        return view('page', $this->getParameters( $this->getDia( 3 ) ) );
    }

    public function intimidade(){
        return view('page', $this->getParameters( $this->getDia( 4 ) ) );
    }

    public function consolo(){
        return view('page', $this->getParameters( $this->getDia( 5 ) ) );
    }

    public function naoDesista(){
        return view('page', $this->getParameters( $this->getDia( 6 ) ) );
    }

    public function missao(){
        return view('page', $this->getParameters( $this->getDia( 7 ) ) );
    }

    public function espiritoSanto(){
        return view('page', $this->getParameters( $this->getDia( 8 ) ) );
    }

    public function fidelidadeDiaria(){
        return view('page', $this->getParameters( $this->getDia( 9 ) ) );
    }

    public function vitoriaFinal(){
        return view('page', $this->getParameters( $this->getDia( 10 ) ) );
    }

    private function getDia( $dia ){
        switch ( $dia ){
            case 1:
                return array(
                    "title" => "FAMÍLIA: Presente de Deus",
                    "goto_url" => "/10DiasDeOracao/FamiliaPresenteDeDeus",
                    "order" => 10,
                    "dia" => 1,
                    "date" => "22 Feb, 2018",
                    "link" => "https://www.youtube.com/embed/jDXnwaBFGfE"
                );
            case 2:
                return array(
                    "order" => 9,
                    "title" => "PERDÃO: Restaurador de Pontes",
                    "goto_url" => "/10DiasDeOracao/PerdaoRestauradorDePontes",
                    "dia" => 2,
                    "date" => "23 Feb, 2018",
                    "link" => "https://www.youtube.com/embed/sDNXidHbgrM"
                );
            case 3:
                return array(
                    "order" => 8,
                    "dia" => 3,
                    "goto_url" => "/10DiasDeOracao/VitoriaSobreATentacao",
                    "title" => "Vitória sobre a tentação",
                    "date" => "24 Feb, 2018",
                    "link" => "https://www.youtube.com/embed/1nKezVObQr4"
                );
            case 4:
                return array(
                    "order" => 7,
                    "goto_url" => "/10DiasDeOracao/IntimidadeComDeusNossaMaior",
                    "title" => "INTIMIDADE COM DEUS: Nossa Maior Necessidade",
                    "dia" => 4,
                    "date" => "25 Feb, 2018",
                    "link" => "https://www.youtube.com/embed/2Ea87YXz8yg"
                );
            case 5:
                return array(
                    "order" => 6,
                    "goto_url" => "/10DiasDeOracao/ConsoloEEsperancaEmDeus",
                    "title" => "Consolo e Esperança em Deus",
                    "dia" => 5,
                    "date" => "26 Feb, 2018",
                    "link" => "https://www.youtube.com/embed/wEPt9IofUp0"
                );
            case 6:
                return array(
                    "order" => 5,
                    "goto_url" => "/10DiasDeOracao/NaoDesistaDeSuaFamilia",
                    "title" => "Não Desista de sua Família",
                    "dia" => 6,
                    "date" => "27 Feb, 2018",
                    "link" => "https://www.youtube.com/embed/0uODOamLYB0"
                );
            case 7:
                return array(
                    "order" => 4,
                    "goto_url" => "/10DiasDeOracao/DoLarParaMissao",
                    "title" => "Do Lar para a Missão",
                    "dia" => 7,
                    "date" => "28 Feb, 2018",
                    "link" => "https://www.youtube.com/embed/nuoku86OrEM"
                );
            case 8:
                return array(
                    "order" => 3,
                    "goto_url" => "/10DiasDeOracao/PoderDoEspiritoSanto",
                    "title" => "O Poder do Espírito Santo",
                    "dia" => 8,
                    "date" => "01 Mar, 2018",
                    "link" => "https://www.youtube.com/embed/kB3jzSJagwI"
                );
            case 9:
                return array(
                    "order" => 2,
                    "goto_url" => "/10DiasDeOracao/FidelidadeDiaria",
                    "title" => "Fidelidade Diária",
                    "dia" => 9,
                    "date" => "02 Mar, 2018",
                    "link" => "https://www.youtube.com/embed/Jf7Z2ULURpU"
                );
            case 10:
                return array(
                    "order" => 1,
                    "goto_url" => "/10DiasDeOracao/VitoriaFinal",
                    "title" => "Vitória Final",
                    "dia" => 10,
                    "date" => "03 Mar, 2018",
                    "link" => "https://www.youtube.com/embed/sMQhINfMO0M"
                );
            default:
                return null;
        }
    }

    public function insert( $dia ){
        $dados = $this->getDia( $dia );

        if( $dados == null || empty( $dados["link"] )){
            return response()->json( array( "response" => "invalid day."), 500 );
        }

        $content = Contents::find(1);
        $content->order = 9;
        $content->save();

        $content = Contents::find(2);
        $content->order = 10;
        $content->save();

        $content = Contents::where( array( "page_id" => 1, "order" => $dados["order"] ) )->first();

        if( $content == null ){
            $imagem = new Imagem();
            $imagem->path = "images/10dias/dia{$dia}.jpg" ;
            $imagem->save();

            $content = new Contents();
            $content->page_id = 1;
            $content->title = "PRIMEIRO DEUS";
            $content->subtitle = $dados["title"];
            $content->imagem_id = $imagem->id;
            $content->order = $dados["order"];
            $content->goto = "Assista";
            $content->goto_url = $dados["goto_url"];
            $content->save();
            return response()->json(array( "response" => "success" ), 200);
        }
        return response()->json(array( "response" => "insert duplicate" ), 500);
    }

    public function delete( $dia ){
        $dados = $this->getDia( $dia );

        $content = Contents::where( array( "page_id" => 1, "order" => $dados["order"] ) )->first();

        if( $content != null ){
            $content->delete();
            Imagem::find( $content->imagem_id)->delete();
            return response()->json(array( "response" => "success" ), 200);
        }
        return response()->json(array( "response" => "fail" ), 404);
    }

    private function getParameters( $retorno ){
        $menus = Menu::all();
        $arvoreMenu = array();
        $arvoreView = array();
        $dataSources = array();

        foreach ( $menus as $item ){
            array_push( $arvoreMenu, array( "url" => substr( $item->url, 1 ), "descricao" => $item->descricao, "view" => $item->view ) );
            array_push( $arvoreView, array( "view" => $item->view ) );
            $dataSources[ $item->view ] = $this->getDataSource( $item );
        }

        return array_merge( $retorno, array( "menu" => $arvoreMenu, "views" => $arvoreView, "dataSources" => $dataSources ) );
    }

    private function getDataSource( $menu ){
        $page = Pages::where( "menu_id", "=", $menu->id )->first();
        if( $page == null ){
            return array( "key" => $menu->key );
        }
        $contents = array();

        foreach( Contents::where( "page_id", "=", $page->id )->cursor() as $content ){
            $content = $content->toArray();

            if( !empty( $content["imagem_id"] ) ){
                $imagem = Imagem::find( $content["imagem_id"] );
                $content["url_imagem"] = $imagem->path;
            }

            array_push( $contents, $content );
        }

        return array( "key" => $menu->key, "title" => $page->title, "subtitle" => $page->subtitle, "contents" => $contents );
    }

    private function getLista(){
        $retorno = array();
        for( $dia = 1; $dia <= 10; $dia++){
            $dadosDia = $this->getDia( $dia );
            if( $dadosDia == null || empty($dadosDia["link"]) ) break;
            array_push( $retorno, $dadosDia );
        }

        return array( "lista" => array_reverse( $retorno ) );
    }
}