<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Menu extends Model
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_id', 'descricao', 'is_show',
    ];

    protected $casts = [
        'is_show' => 'boolean',
        'parent_id' => 'int',
    ];

    protected $dateFormat = 'U';

    public function childrenMenus(){
        return $this->hasMany( "Menu", "parent_id", "id" );
    }

    public function allChildrenMenus(){
        return $this->childrenMenus()->with('allChildrenMenus');
    }

    public function scopeChildless($q)
    {
        $q->has('childrenAccounts', '=', 0);
    }
}
