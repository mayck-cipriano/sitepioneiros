<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Clube de Desbravadores Pioneiros da Colina</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">

        <!-- Fix Internet Explorer ______________________________________-->

        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <script src="{{ asset('vendor/html5shiv.js') }}"></script>
        <script src="{{ asset('vendor/respond.js') }}"></script>
        <![endif]-->
    </head>
    <!-- Hospedado na Hostgator -->
    <body>
        <div class="main-page-wrapper">
            @include('partials.menu')
            @foreach ($views as $v)
                @include( $v["view"], $dataSources[ $v["view"] ] )
            @endforeach
            @include('partials.footer')
        </div> <!-- /.main-page-wrapper -->
    </body>
</html>
