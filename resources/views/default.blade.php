<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- For IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- For Resposive Device -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ $title }} - Clube de Desbravadores Pioneiros da Colina</title>

    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="56x56" href="images/fav-icon/icon.png">


    <!-- Main style sheet -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    <!-- responsive style sheet -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}">
    <!-- components style sheet -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/components.css') }}">


    <!-- Fix Internet Explorer ______________________________________-->

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="{{ asset('vendor/html5shiv.js') }}"></script>
    <script src="{{ asset('vendor/respond.js') }}"></script>
    <![endif]-->

</head>

<body>
<div class="main-page-wrapper">
    @include('partials.menu')

    <!--
    =====================================================
        Theme Inner page Banner
    =====================================================
    -->
    <section class="inner-page-banner">
        <div class="opacity">
            <div class="container">
                @include('partials.path' )
            </div> <!-- /.container -->
        </div> <!-- /.opacity -->
    </section> <!-- /.inner-page-banner -->

    @include( $view )
    @include('partials.footer')
    </div> <!-- /.main-page-wrapper -->
</body>
</html>