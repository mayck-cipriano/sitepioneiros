<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- For IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- For Resposive Device -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Clube de Desbravadores Pioneiros da Colina</title>

    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="56x56" href="images/fav-icon/icon.png">


    <!-- Main style sheet -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    <!-- responsive style sheet -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}">


    <!-- Fix Internet Explorer ______________________________________-->

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="{{ asset('vendor/html5shiv.js') }}"></script>
    <script src="{{ asset('vendor/respond.js') }}"></script>
    <![endif]-->

</head>

<body>
<div class="main-page-wrapper">
    @include('partials.menu')

    <!--
    =====================================================
        Theme Inner page Banner
    =====================================================
    -->
    <section class="inner-page-banner">
        <div class="opacity">
            <div class="container">
                <!-- <h2>Blog</h2> -->
                <ul>
                    <li><a href="/">Home</a></li>
                    <li>/</li>
                    <li><a href="/10DiasDeOracao">10 Dias de Oração</a></li>
                    <li>/</li>
                    <li>Dia {{ $dia  }}</li>
                </ul>
            </div> <!-- /.container -->
        </div> <!-- /.opacity -->
    </section> <!-- /.inner-page-banner -->



    <!--
    =====================================================
        Blog Page Details
    =====================================================
    -->
    <article class="blog-details-page">
        <div class="container">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix">
                <div class="blog-details-post-wrapper">
                    <iframe width="560" height="315" src="{{ $link  }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    <div class="post-heading">
                        <h4>{{ $title  }}</h4>
                        <span>Posted by <a href="#" class="tran3s p-color">admin</a>  at {{ $date }}</span>
                    </div> <!-- /.post-heading -->
                    <p style="display: none;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Eceeur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <h6 style="display: none;">Entrepreneurship Program</h6>
                    <p style="display: none;">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model setence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                    <div class="row" style="display: none;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <p><i class="fa fa-quote-left" aria-hidden="true"></i>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cuidtat non proident, sunt in culpa qui officia.<i class="fa fa-quote-right" aria-hidden="true"></i></p>
                        </div> <!-- /.col -->
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <img src="images/blog/9.jpg" alt="Image">
                        </div> <!-- /.col- -->
                    </div> <!-- /.row -->

                    <p style="display: none;">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard Mclintock, a Latin professor at Sydney College in Virginia, looked up one of the more obscure Latin words.</p>
                    <div class="clear-fix list-img-wrapper" style="display: none;">
                        <img src="images/blog/10.jpg" alt="Image" class="float-left">
                        <ul class="float-left">
                            <li><a href="#" class="tran3s"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Get samples of your product.</a></li>
                            <li><a href="#" class="tran3s"><i class="fa fa-hand-o-right" aria-hidden="true"></i> If you're just starting out.</a></li>
                            <li><a href="#" class="tran3s"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Sign up for an email service.</a></li>
                            <li><a href="#" class="tran3s"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Publicize your publicity.</a></li>
                            <li><a href="#" class="tran3s"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Plan your attack.</a></li>
                            <li><a href="#" class="tran3s"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Learn to ask for referrals.</a></li>
                            <li><a href="#" class="tran3s"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Ask for work or leads.</a></li>
                        </ul>
                    </div> <!-- /.list-img-wrapper -->


                    <div class="clear-fix post-share-area" style="display: none;">
                        <ul class="share float-left">
                            <li><a href="#" class="tran3s active">Business,</a></li>
                            <li><a href="#" class="tran3s">Advertising ,</a></li>
                            <li><a href="#" class="tran3s">Marketing,</a></li>
                            <li><a href="#" class="tran3s">Optimization</a></li>
                        </ul>
                        <ul class="share-icon float-right">
                            <li>Share:</li>
                            <li><a href="#" class="tran3s round-border"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="tran3s round-border"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="tran3s round-border"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="tran3s round-border"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                        </ul>
                    </div> <!-- /post-share-area -->


                    <div class="comment-area"  style="display: none;">
                        <h4>2 COMMENTS</h4>

                        <div class="comment-wrapper">
                            <div class="single-comment clear-fix">
                                <img src="images/blog/11.jpg" alt="Image" class="float-left">
                                <div class="comment float-left">
                                    <h6>Prodip Ghosh</h6>
                                    <i>Feb 28, 2017</i>

                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
                                    <button class="tran3s">Reply</button>
                                </div> <!-- /.comment -->
                            </div> <!-- /.single-comment -->
                            <div class="single-comment clear-fix reply-comment">
                                <img src="images/blog/12.jpg" alt="Image" class="float-left">
                                <div class="comment float-left">
                                    <h6>Angela Collins</h6>
                                    <i>Feb 28, 2017</i>

                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor inciunt ut labore et dolore magna aliqua.</p>
                                    <button class="tran3s">Reply</button>
                                </div> <!-- /.comment -->
                            </div> <!-- /.single-comment -->
                            <div class="single-comment clear-fix">
                                <img src="images/blog/13.jpg" alt="Image" class="float-left">
                                <div class="comment float-left">
                                    <h6>Sumon Rahman</h6>
                                    <i>Feb 28, 2017</i>

                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
                                    <button class="tran3s">Reply</button>
                                </div> <!-- /.comment -->
                            </div> <!-- /.single-comment -->
                        </div> <!-- /.comment-wrapper -->
                    </div> <!-- /.comment-area -->


                    <div class="post-comment"  style="display: none;">
                        <h4>Post A Comment</h4>

                        <form action="#" method="post">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span>Name*</span>
                                    <div class="single-input">
                                        <input type="text">
                                    </div> <!-- /.single-input -->
                                </div> <!-- /.col- -->
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <span>Email*</span>
                                    <div class="single-input">
                                        <input type="email">
                                    </div> <!-- /.single-input -->
                                </div> <!-- /.col- -->

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <span>Your Comment</span>
                                    <div class="single-input">
                                        <textarea></textarea>
                                    </div> <!-- /.single-input -->
                                </div> <!-- /.col- -->
                            </div> <!-- /.row -->

                            <button class="tran3s p-color-bg">Post Comment</button>
                        </form>
                    </div> <!-- /.post-comment -->
                </div> <!-- /.blog-details-post-wrapper -->
            </div> <!-- /.col- -->

            <!-- ========================== Aside Bar ======================== -->
            <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12 p-fix" style="display: none;">
                <aside>
                    <form action="#" class="sidebar-search-box">
                        <input type="text" placeholder="Search...">
                        <button class="p-color-bg"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </form>

                    <div class="sidebar-news-list">
                        <h6>Latest News</h6>
                        <ul>
                            <li><a href="#" class="tran3s active"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Google and Your Business</a></li>
                            <li><a href="#" class="tran3s"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Small Business Trends</a></li>
                            <li><a href="#" class="tran3s"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Aweber Communications</a></li>
                            <li><a href="#" class="tran3s"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Planning Start-up Stories</a></li>
                            <li><a href="#" class="tran3s"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Learn About Six Business</a></li>
                            <li><a href="#" class="tran3s"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Succeed As Your Own Boss</a></li>
                            <li><a href="#" class="tran3s"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Guardian Small Business Network</a></li>
                            <li><a href="#" class="tran3s"><i class="fa fa-hand-o-right" aria-hidden="true"></i> KPMG Small Business Accounting</a></li>
                            <li><a href="#" class="tran3s"><i class="fa fa-hand-o-right" aria-hidden="true"></i> CorpNet - The Startup Starting Line</a></li>
                            <li><a href="#" class="tran3s"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Tips for Successful Marketing</a></li>
                            <li><a href="#" class="tran3s"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Microsoft US Small and Midsize Business</a></li>
                        </ul>
                    </div> <!-- /.sidebar-news-list -->

                    <div class="sidebar-calendar">
                        <h6>Calendar</h6>
                        <div class="monthly" id="blog-calendar"></div>
                    </div> <!-- /.sidebar-calendar -->

                    <div class="sidebar-archives">
                        <h6>Archives</h6>
                        <ul>
                            <li><a href="#" class="tran3s active"><i class="fa fa-hand-o-right" aria-hidden="true"></i> February 2017</a></li>
                            <li><a href="#" class="tran3s"><i class="fa fa-hand-o-right" aria-hidden="true"></i> January 2017</a></li>
                            <li><a href="#" class="tran3s"><i class="fa fa-hand-o-right" aria-hidden="true"></i> November 2016</a></li>
                            <li><a href="#" class="tran3s"><i class="fa fa-hand-o-right" aria-hidden="true"></i> June 2016</a></li>
                            <li><a href="#" class="tran3s"><i class="fa fa-hand-o-right" aria-hidden="true"></i> May 2016</a></li>
                        </ul>
                    </div> <!-- /.sidebar-archives -->

                    <div class="sidebar-recent-post">
                        <h6>RECENT POST</h6>

                        <div class="recent-single-post clear-fix">
                            <img src="images/blog/4.jpg" alt="Image" class="float-left">
                            <div class="post float-left">
                                <a href="#" class="tran3s">Playback: Akufo-Addo speaks to business community in United States.</a>
                                <span>January 18, 2017</span>
                            </div> <!-- /.post -->
                        </div> <!-- /.recent-single-post -->

                        <div class="recent-single-post clear-fix">
                            <img src="images/blog/5.jpg" alt="Image" class="float-left">
                            <div class="post float-left">
                                <a href="#" class="tran3s">Playback: Akufo-Addo speaks to business community in United States.</a>
                                <span>January 18, 2017</span>
                            </div> <!-- /.post -->
                        </div> <!-- /.recent-single-post -->

                        <div class="recent-single-post clear-fix">
                            <img src="images/blog/6.jpg" alt="Image" class="float-left">
                            <div class="post float-left">
                                <a href="#" class="tran3s">Playback: Akufo-Addo speaks to business community in United States.</a>
                                <span>January 18, 2017</span>
                            </div> <!-- /.post -->
                        </div> <!-- /.recent-single-post -->

                        <div class="recent-single-post clear-fix">
                            <img src="images/blog/7.jpg" alt="Image" class="float-left">
                            <div class="post float-left">
                                <a href="#" class="tran3s">Playback: Akufo-Addo speaks to business community in United States.</a>
                                <span>January 18, 2017</span>
                            </div> <!-- /.post -->
                        </div> <!-- /.recent-single-post -->
                    </div> <!-- /.sidebar-recent-post -->

                    <div class="sidebar-tags">
                        <h6>Tags</h6>
                        <ul>
                            <li><a href="#" class="tran3s">Development</a></li>
                            <li><a href="#" class="tran3s">Advertising</a></li>
                            <li><a href="#" class="tran3s">Design</a></li>
                            <li><a href="#" class="tran3s">Seo</a></li>
                            <li><a href="#" class="tran3s">Marketing</a></li>
                            <li><a href="#" class="tran3s">html</a></li>
                            <li><a href="#" class="tran3s">ppc</a></li>
                            <li><a href="#" class="tran3s">Business</a></li>
                            <li><a href="#" class="tran3s">WordPress</a></li>
                            <li><a href="#" class="tran3s">Optimization</a></li>
                        </ul>
                    </div> <!-- /.sidebar-tags -->
                </aside>
            </div> <!-- /.col- -->
        </div> <!-- /.container -->
    </article>


        @include('partials.footer')
    </div> <!-- /.main-page-wrapper -->
</body>
</html>