@if( isset( $contents ) )
    <div id="unidades" class="service-section">
        <div class="container">
            <div class="theme-title">
                <h2>Nossas Unidades</h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p>
            </div> <!-- /.theme-title -->

            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="single-service-content">
                        <div class="icon-heading tran3s">
                            <div class="icon tran3s"><i class="fa fa-paint-brush" aria-hidden="true"></i></div>
                            <h6><a href="#" class="tran3s">Web Design</a></h6>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur scing elit, sed do eiusmod, tempor incididunt labore et dolore magna aliqua. Ut enim ad minim ut veniam, quis nostrud exercitation ullamco aliquip ex ea commodo consequat. </p>
                    </div> <!-- /.single-service-content -->
                </div> <!-- /.col-lg -->

                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="single-service-content">
                        <div class="icon-heading tran3s">
                            <div class="icon tran3s"><i class="fa fa-camera" aria-hidden="true"></i></div>
                            <h6><a href="#" class="tran3s">Photography</a></h6>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur scing elit, sed do eiusmod, tempor incididunt labore et dolore magna aliqua. Ut enim ad minim ut veniam, quis nostrud exercitation ullamco aliquip ex ea commodo consequat. </p>
                    </div> <!-- /.single-service-content -->
                </div> <!-- /.col-lg -->

                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="single-service-content">
                        <div class="icon-heading tran3s">
                            <div class="icon tran3s"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></div>
                            <h6><a href="#" class="tran3s">Web Development</a></h6>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur scing elit, sed do eiusmod, tempor incididunt labore et dolore magna aliqua. Ut enim ad minim ut veniam, quis nostrud exercitation ullamco aliquip ex ea commodo consequat. </p>
                    </div> <!-- /.single-service-content -->
                </div> <!-- /.col-lg -->

                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="single-service-content">
                        <div class="icon-heading tran3s">
                            <div class="icon tran3s"><i class="fa fa-line-chart" aria-hidden="true"></i></div>
                            <h6><a href="#" class="tran3s">Online Marketing</a></h6>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur scing elit, sed do eiusmod, tempor incididunt labore et dolore magna aliqua. Ut enim ad minim ut veniam, quis nostrud exercitation ullamco aliquip ex ea commodo consequat. </p>
                    </div> <!-- /.single-service-content -->
                </div> <!-- /.col-lg -->

                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="single-service-content">
                        <div class="icon-heading tran3s">
                            <div class="icon tran3s"><i class="fa fa-life-ring" aria-hidden="true"></i></div>
                            <h6><a href="#" class="tran3s">Digital Media</a></h6>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur scing elit, sed do eiusmod, tempor incididunt labore et dolore magna aliqua. Ut enim ad minim ut veniam, quis nostrud exercitation ullamco aliquip ex ea commodo consequat. </p>
                    </div> <!-- /.single-service-content -->
                </div> <!-- /.col-lg -->

                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="single-service-content">
                        <div class="icon-heading tran3s">
                            <div class="icon tran3s"><i class="fa fa-anchor" aria-hidden="true"></i></div>
                            <h6><a href="#" class="tran3s">Support</a></h6>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur scing elit, sed do eiusmod, tempor incididunt labore et dolore magna aliqua. Ut enim ad minim ut veniam, quis nostrud exercitation ullamco aliquip ex ea commodo consequat. </p>
                    </div> <!-- /.single-service-content -->
                </div> <!-- /.col-lg -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /#service-section -->
@endif