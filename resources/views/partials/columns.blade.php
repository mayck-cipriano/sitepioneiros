@if( isset( $contents ) )
<section id="{{ $key  }}" class="about-us">
    <div class="container">
        <div class="theme-title">
            <h2>{{$title}}</h2>
            <p>{{$subtitle}}</p>
        </div> <!-- /.theme-title -->

        <div class="row">
            @foreach( $contents as $item)
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 column-component">
                <div class="single-about-content">
                    @if( isset( $item["icon"] ) )
                    <div class="icon round-border tran3s">
                        <i class="{{ $item["icon"] }}" aria-hidden="true"></i>
                    </div>
                    @endif
                    <h5><a href="#" class="tran3s">{{ $item["title"] }}</a></h5>
                    <p>{{ $item["subtitle"] }}</p>
                    @if( isset( $item["goto"]) )
                    <a href="{{ $item["goto_url"] }}" class="more tran3s hvr-bounce-to-right">{{ $item["goto"] }}</a>
                    @endif
                </div> <!-- /.single-about-content -->
            </div> <!-- /.col -->
            @endforeach
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</section> <!-- /#about-us -->
@endif