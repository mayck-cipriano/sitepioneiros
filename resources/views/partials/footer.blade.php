<!--
=====================================================
    Footer
=====================================================
-->
<footer>
    <div class="container">
        <a href="/" class="logo"><img src="{{ asset('images/logo/logo.png')  }}" alt="Logo"></a>

        <ul>
            <li><a href="https://www.facebook.com/pioneirosdacolina.unasp/" class="tran3s round-border"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <li><a href="https://www.instagram.com/pioneirosdacolina/" class="tran3s round-border"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
            <!--
            <li><a href="#" class="tran3s round-border"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
            <li><a href="#" class="tran3s round-border"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
            <li><a href="#" class="tran3s round-border"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
            <li><a href="#" class="tran3s round-border"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
            <li><a href="#" class="tran3s round-border"><i class="fa fa-flickr" aria-hidden="true"></i></a></li>
            <li><a href="#" class="tran3s round-border"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
            <li><a href="#" class="tran3s round-border"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
            -->
        </ul>
        <p>Copyright @2018 All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a></p>
    </div>
</footer>

<!-- =============================================
    Loading Transition
============================================== -->
<div id="loader-wrapper">
    <div id="preloader_1">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>


<!-- Scroll Top Button -->
<button class="scroll-top tran3s p-color-bg">
    <i class="fa fa-long-arrow-up" aria-hidden="true"></i>
</button>

<!-- Js File_________________________________ -->

<!-- j Query -->
<script type="text/javascript" src="{{ asset('vendor/jquery.2.2.3.min.js') }}"></script>

<!-- Bootstrap JS -->
<script type="text/javascript" src="{{ asset('vendor/bootstrap/bootstrap.min.js') }}"></script>

<!-- Vendor js _________ -->

<!-- revolution -->
<script src="{{ asset('vendor/revolution/jquery.themepunch.tools.min.js') }}"></script>
<script src="{{ asset('vendor/revolution/jquery.themepunch.revolution.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/revolution/revolution.extension.slideanims.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/revolution/revolution.extension.layeranimation.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/revolution/revolution.extension.navigation.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/revolution/revolution.extension.kenburn.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/revolution/revolution.extension.actions.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/revolution/revolution.extension.video.min.js') }}"></script>

<!-- Google map js -->
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZ8VrXgGZ3QSC-0XubNhuB2uKKCwqVaD0&callback=goMap" type="text/javascript"></script> <!-- Gmap Helper -->
<script src="{{ asset('vendor/gmaps.min.js') }}"></script>
<!-- owl.carousel -->
<script type="text/javascript" src="{{ asset('vendor/owl-carousel/owl.carousel.min.js') }}"></script>
<!-- mixitUp -->
<script type="text/javascript" src="{{ asset('vendor/jquery.mixitup.min.js') }}"></script>
<!-- Progress Bar js -->
<script type="text/javascript" src="{{ asset('vendor/skills-master/jquery.skills.js') }}"></script>
<!-- Validation -->
<script type="text/javascript" src="{{ asset('vendor/contact-form/validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/contact-form/jquery.form.js') }}"></script>


<!-- Theme js -->
<script type="text/javascript" src="{{ asset('js/theme.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/map-script.js') }}"></script>
