    <!--
    =====================================================
        Blog Page Details
    =====================================================
    -->
    <article class="blog-details-page">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-fix">
                <div class="blog-details-post-wrapper">
                    @foreach( $contents as $item )
                        {!!$item["description"]!!}
                    @endforeach

                </div> <!-- /.blog-details-post-wrapper -->
            </div> <!-- /.col- -->
        </div> <!-- /.container -->
    </article>