<!--
=====================================================
    Blog Section
=====================================================
-->
@if( isset( $contents ) )
<div id="eventos" class="blog-section">
    <div class="container">
        <div class="theme-title">
            <h2>OUR LATEST Blog</h2>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p>
        </div> <!-- /.theme-title -->

        <div class="clear-fix">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="single-news-item">
                    <div class="img"><img src="images/blog/1.jpg" alt="Image">
                        <a href="blog-details.html" class="opacity tran4s"><i class="fa fa-link" aria-hidden="true"></i></a>
                    </div> <!-- /.img -->

                    <div class="post">
                        <h6><a href="blog-details.html" class="tran3s">Playback: Akufo-Addo speaks to business community</a></h6>
                        <a href="blog-details.html">Posted by <span class="p-color">admin</span>  at 04 Feb, 2017</a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sagittis iaculis velit in tristique. Curabitur ac urna urna sed accumsan... <a href="blog-details.html" class="tran3s">Read More</a></p>
                    </div> <!-- /.post -->
                </div> <!-- /.single-news-item -->
            </div> <!-- /.col- -->

            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="single-news-item">
                    <div class="img"><img src="images/blog/2.jpg" alt="Image">
                        <a href="blog-details.html" class="opacity tran4s"><i class="fa fa-link" aria-hidden="true"></i></a>
                    </div> <!-- /.img -->

                    <div class="post">
                        <h6><a href="blog-details.html" class="tran3s">Playback: Akufo-Addo speaks to business community</a></h6>
                        <a href="blog-details.html">Posted by <span class="p-color">admin</span>  at 04 Feb, 2017</a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sagittis iaculis velit in tristique. Curabitur ac urna urna sed accumsan... <a href="blog-details.html" class="tran3s">Read More</a></p>
                    </div> <!-- /.post -->
                </div> <!-- /.single-news-item -->
            </div> <!-- /.col- -->

            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="single-news-item">
                    <div class="img"><img src="images/blog/3.jpg" alt="Image">
                        <a href="blog-details.html" class="opacity tran4s"><i class="fa fa-link" aria-hidden="true"></i></a>
                    </div> <!-- /.img -->

                    <div class="post">
                        <h6><a href="blog-details.html" class="tran3s">Playback: Akufo-Addo speaks to business community</a></h6>
                        <a href="blog-details.html">Posted by <span class="p-color">admin</span>  at 04 Feb, 2017</a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sagittis iaculis velit in tristique. Curabitur ac urna urna sed accumsan... <a href="blog-details.html" class="tran3s">Read More</a></p>
                    </div> <!-- /.post -->
                </div> <!-- /.single-news-item -->
            </div> <!-- /.col- -->
        </div> <!-- /.clear-fix -->
    </div> <!-- /.container -->
</div> <!-- /#blog-section -->
@include('partials.partner')
@endif