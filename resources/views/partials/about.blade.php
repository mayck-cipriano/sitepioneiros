<!--
=====================================================
    About us Section
=====================================================
-->
@if( isset( $contents ) )
<section id="about-us">
    <div class="container">
        <div class="theme-title">
            <h2>SOBRE NÓS</h2>
            <p>Nossa história, seja como Desbravador ou como Pioneiros da Colina, é incrível e cheia de providência divina. Aproveite para conhecer um pouco sobre nós, do passado até hoje.</p>
        </div> <!-- /.theme-title -->

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="single-about-content">
                    <div class="icon round-border tran3s">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    </div>
                    <h5><a href="#" class="tran3s">Ao redor do mundo</a></h5>
                    <p>Uma pequena idéia, iniciada em paralelo ao redor dos Estados Unidos, muda o relacionamento de juvenis e adolescentes com a natureza e o seu Criador..</p>
                    <a href="#" class="more tran3s hvr-bounce-to-right">Saiba Mais</a>
                </div> <!-- /.single-about-content -->
            </div> <!-- /.col -->

            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="single-about-content">
                    <div class="icon round-border tran3s">
                        <i class="fa fa-camera" aria-hidden="true"></i>
                    </div>
                    <h5><a href="#" class="tran3s">AO CHEGAR NA COLINA</a></h5>
                    <p>Fábrica de teologandos, a colina é conhecida por centro de formação de pastores para levar à mensagem da breve volta de Jesus. Um destes, aceita o chamado de evangelhizar os mais novos.</p>
                    <a href="#" class="more tran3s hvr-bounce-to-right">Saiba Mais</a>
                </div> <!-- /.single-about-content -->
            </div> <!-- /.col -->
<!--
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="single-about-content">
                    <div class="icon round-border tran3s">
                        <i class="fa fa-life-ring" aria-hidden="true"></i>
                    </div>
                    <h5><a href="#" class="tran3s">Digital Media</a></h5>
                    <p>Lorem ipsum dolor sit amet, consect et adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                    <a href="#" class="more tran3s hvr-bounce-to-right">More Details</a>
                </div>
            </div> <!-- /.col -->
-->
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="single-about-content">
                    <div class="icon round-border tran3s">
                        <i class="fa fa-line-chart" aria-hidden="true"></i>
                    </div>
                    <h5><a href="#" class="tran3s">Nos dias Atuais</a></h5>
                    <p>Nessa seção você saberá mais sobre a nossa agenda, comunicados, documentos úteis e outras informações atuais.</p>
                    <a href="#" class="more tran3s hvr-bounce-to-right">Saiba Mais</a>
                </div> <!-- /.single-about-content -->
            </div> <!-- /.col -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</section> <!-- /#about-us -->
@endif