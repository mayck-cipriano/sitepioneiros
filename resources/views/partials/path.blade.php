@if( isset( $path ) )
    <ul>
        @for( $i = 0; $i< count( $path ); $i++ )
            @if( $i > 0 )
                <li>/</li>
            @endif
            @if( isset( $path[$i]["url"] ) && !empty( $path[$i]["url"] ) )
                <li><a href="{{ $path[$i]["url"] }}">{{ $path[$i]["label"] }}</a></li>
            @else
                <li>{{ $path[$i]["label"] }}</li>
            @endif
        @endfor
    </ul>
@endif
