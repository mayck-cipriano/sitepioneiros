<!--
=====================================================
    Page middle banner
=====================================================
-->

<div class="page-middle-banner">
    <div class="opacity">
        <h3>We Create Creative <span class="p-color">&amp;</span> Best Unique Design</h3>
        <a href="#" class="hvr-bounce-to-right">Let's Work Together</a>
    </div> <!-- /.opacity -->
</div> <!-- /.page-middle-banner -->
