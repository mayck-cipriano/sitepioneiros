<!--
=====================================================
    Partner Section
=====================================================
-->
<div id="partner-section">
    <center>
        <div class="container">
            <div id="partner_logo" class="owl-carousel owl-theme">
                <div class="item"><a href="https://www.adventistas.org/pt/" target="_blank"><img src="images/logo/p1.png" alt="Igreja Adventista do Sétimo dia"></a></div>
                <div class="item"><a href="http://unasp-sp.edu.br/" target="_blank"><img src="images/logo/p2.png" alt="UNASP Campus São Paulo"></a></div>
            </div> <!-- End .partner_logo -->
        </div> <!-- /.container -->
    </center>
</div> <!-- /#partner-section -->