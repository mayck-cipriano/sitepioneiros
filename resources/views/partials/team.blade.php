@if( isset( $contents ) )
    <!--
    =====================================================
        Team Section
    =====================================================
    -->
    <div id="diretoria" class="team-section">
        <div class="container">
            <div class="theme-title">
                <h2>Diretoria</h2>
                <p>Para cuidar desse exército de meninos e meninas, temos um time de mais de 100 pessoas. Aqui, temos listados apenas a diretoria executiva. Quanto aos outros membros, entre nas nossas unidades..</p>
            </div> <!-- /.theme-title -->

            <div class="clear-fix team-member-wrapper">
                <div class="float-left">
                    <div class="single-team-member">
                        <div class="img">
                            <img src="images/team/1.jpg" alt="Image">
                            <div class="opacity tran4s">
                                <h4>Gonzalez Gina</h4>
                                <span>Web Developer</span>
                                <p>On the other hand, We denounce ut with righteo indignation and dislike men who are so beguiled and demoralized.</p>
                            </div>
                        </div> <!-- /.img -->
                        <div class="member-name">
                            <h6>Gonzalez Gina</h6>
                            <p>Web Developer</p>
                            <ul>
                                <li><a href="#" class="tran3s round-border"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#" class="tran3s round-border"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="#" class="tran3s round-border"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                <li><a href="#" class="tran3s round-border"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            </ul>
                        </div> <!-- /.member-name -->
                    </div> <!-- /.single-team-member -->
                </div> <!-- /float-left -->

                <div class="float-left">
                    <div class="single-team-member">
                        <div class="img">
                            <img src="images/team/2.jpg" alt="Image">
                            <div class="opacity tran4s">
                                <h4>Holly Vincenzini</h4>
                                <span>Media Partner</span>
                                <p>On the other hand, We denounce ut with righteo indignation and dislike men who are so beguiled and demoralized.</p>
                            </div>
                        </div> <!-- /.img -->
                        <div class="member-name">
                            <h6>Holly Vincenzini</h6>
                            <p>Media Partner</p>
                            <ul>
                                <li><a href="#" class="tran3s round-border"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#" class="tran3s round-border"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="#" class="tran3s round-border"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                <li><a href="#" class="tran3s round-border"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            </ul>
                        </div> <!-- /.member-name -->
                    </div> <!-- /.single-team-member -->
                </div> <!-- /float-left -->

                <div class="float-left">
                    <div class="single-team-member">
                        <div class="img">
                            <img src="images/team/3.jpg" alt="Image">
                            <div class="opacity tran4s">
                                <h4>Ramirez Minita</h4>
                                <span>Graphic Design</span>
                                <p>On the other hand, We denounce ut with righteo indignation and dislike men who are so beguiled and demoralized.</p>
                            </div>
                        </div> <!-- /.img -->
                        <div class="member-name">
                            <h6>Ramirez Minita</h6>
                            <p>Graphic Design</p>
                            <ul>
                                <li><a href="#" class="tran3s round-border"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#" class="tran3s round-border"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="#" class="tran3s round-border"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                <li><a href="#" class="tran3s round-border"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            </ul>
                        </div> <!-- /.member-name -->
                    </div> <!-- /.single-team-member -->
                </div> <!-- /float-left -->
            </div> <!-- /.team-member-wrapper -->
        </div> <!-- /.conatiner -->
    </div> <!-- /#team-section -->
    @include('partials.skills')
@endif