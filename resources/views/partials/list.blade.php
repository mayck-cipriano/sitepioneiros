
    @foreach( $contents as $item)
    <article class="blog-details-page">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-fix">
                <div class="blog-details-post-wrapper">
                    <div class="post-heading">
                        <h4><a href="{{ $item["goto_url"]  }}">{{ $item["title"]  }}</a></h4>
                        <span>Posted by <a href="#" class="tran3s p-color">admin</a>  at {{ $item["created_at"] }}</span>
                    </div>
                </div> <!-- /.blog-details-post-wrapper -->
            </div> <!-- /.col- -->
        </div> <!-- /.container -->
    </article>
    @endforeach