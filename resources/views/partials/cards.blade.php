<!--
=====================================================
    Project Section
=====================================================
-->
@if( isset( $contents ) )
<div id="galeria" class="cards-section">
    <div class="container">
        <div class="theme-title">
            <h2>Eventos</h2>
            <p>Veja algumas fotos dos nossos eventos.</p>
        </div> <!-- /.theme-title -->

        <div class="project-menu">
            <ul>
                <li class="filter active tran3s" data-filter="all">All</li>
                <li class="filter tran3s" data-filter=".web">Web Design</li>
                <li class="filter tran3s" data-filter=".photo">Photography</li>
                <li class="filter tran3s" data-filter=".webd">Web Development</li>
                <li class="filter tran3s" data-filter=".om">Online Marketing</li>
                <li class="filter tran3s" data-filter=".dmedia">Digital Media</li>
                <li class="filter tran3s" data-filter=".support">Support</li>
            </ul>
        </div>

        <div class="project-gallery clear-fix">
            <div class="mix grid-item photo om dmedia">
                <div class="single-img">
                    <img src="images/project/1.jpg" alt="Image">
                    <div class="opacity">
                        <div class="border-shape"><div><div>
                                    <h6><a href="#">Sweet Photo</a></h6>
                                    <ul>
                                        <li>Business /</li>
                                        <li>Service /</li>
                                        <li>Product /</li>
                                        <li>Template</li>
                                    </ul></div></div>
                        </div> <!-- /.border-shape -->
                    </div> <!-- /.opacity -->
                </div> <!-- /.single-img -->
            </div> <!-- /.grid-item -->

            <div class="mix grid-item web webd dmedia support">
                <div class="single-img">
                    <img src="images/project/2.jpg" alt="Image">
                    <div class="opacity">
                        <div class="border-shape"><div><div>
                                    <h6><a href="#">Sweet Photo</a></h6>
                                    <ul>
                                        <li>Business /</li>
                                        <li>Service /</li>
                                        <li>Product /</li>
                                        <li>Template</li>
                                    </ul></div></div>
                        </div> <!-- /.border-shape -->
                    </div> <!-- /.opacity -->
                </div> <!-- /.single-img -->
            </div> <!-- /.grid-item -->

            <div class="mix grid-item photo webd support">
                <div class="single-img">
                    <img src="images/project/3.jpg" alt="Image">
                    <div class="opacity">
                        <div class="border-shape"><div><div>
                                    <h6><a href="#">Sweet Photo</a></h6>
                                    <ul>
                                        <li>Business /</li>
                                        <li>Service /</li>
                                        <li>Product /</li>
                                        <li>Template</li>
                                    </ul></div></div>
                        </div> <!-- /.border-shape -->
                    </div> <!-- /.opacity -->
                </div> <!-- /.single-img -->
            </div> <!-- /.grid-item -->

            <div class="mix grid-item web om">
                <div class="single-img">
                    <img src="images/project/4.jpg" alt="Image">
                    <div class="opacity">
                        <div class="border-shape"><div><div>
                                    <h6><a href="#">Sweet Photo</a></h6>
                                    <ul>
                                        <li>Business /</li>
                                        <li>Service /</li>
                                        <li>Product /</li>
                                        <li>Template</li>
                                    </ul></div></div>
                        </div> <!-- /.border-shape -->
                    </div> <!-- /.opacity -->
                </div> <!-- /.single-img -->
            </div> <!-- /.grid-item -->

            <div class="mix grid-item photo webd dmedia om">
                <div class="single-img">
                    <img src="images/project/5.jpg" alt="Image">
                    <div class="opacity">
                        <div class="border-shape"><div><div>
                                    <h6><a href="#">Sweet Photo</a></h6>
                                    <ul>
                                        <li>Business /</li>
                                        <li>Service /</li>
                                        <li>Product /</li>
                                        <li>Template</li>
                                    </ul></div></div>
                        </div> <!-- /.border-shape -->
                    </div> <!-- /.opacity -->
                </div> <!-- /.single-img -->
            </div> <!-- /.grid-item -->

            <div class="mix grid-item web webd dmedia om">
                <div class="single-img">
                    <img src="images/project/6.jpg" alt="Image">
                    <div class="opacity">
                        <div class="border-shape"><div><div>
                                    <h6><a href="#">Sweet Photo</a></h6>
                                    <ul>
                                        <li>Business /</li>
                                        <li>Service /</li>
                                        <li>Product /</li>
                                        <li>Template</li>
                                    </ul></div></div>
                        </div> <!-- /.border-shape -->
                    </div> <!-- /.opacity -->
                </div> <!-- /.single-img -->
            </div> <!-- /.grid-item -->

            <div class="mix grid-item photo om support">
                <div class="single-img">
                    <img src="images/project/7.jpg" alt="Image">
                    <div class="opacity">
                        <div class="border-shape"><div><div>
                                    <h6><a href="#">Sweet Photo</a></h6>
                                    <ul>
                                        <li>Business /</li>
                                        <li>Service /</li>
                                        <li>Product /</li>
                                        <li>Template</li>
                                    </ul></div></div>
                        </div> <!-- /.border-shape -->
                    </div> <!-- /.opacity -->
                </div> <!-- /.single-img -->
            </div> <!-- /.grid-item -->

            <div class="mix grid-item web support">
                <div class="single-img">
                    <img src="images/project/8.jpg" alt="Image">
                    <div class="opacity">
                        <div class="border-shape"><div><div>
                                    <h6><a href="#">Sweet Photo</a></h6>
                                    <ul>
                                        <li>Business /</li>
                                        <li>Service /</li>
                                        <li>Product /</li>
                                        <li>Template</li>
                                    </ul></div></div>
                        </div> <!-- /.border-shape -->
                    </div> <!-- /.opacity -->
                </div> <!-- /.single-img -->
            </div> <!-- /.grid-item -->

            <div class="mix grid-item photo webd dmedia support">
                <div class="single-img">
                    <img src="images/project/9.jpg" alt="Image">
                    <div class="opacity">
                        <div class="border-shape"><div><div>
                                    <h6><a href="#">Sweet Photo</a></h6>
                                    <ul>
                                        <li>Business /</li>
                                        <li>Service /</li>
                                        <li>Product /</li>
                                        <li>Template</li>
                                    </ul></div></div>
                        </div> <!-- /.border-shape -->
                    </div> <!-- /.opacity -->
                </div> <!-- /.single-img -->
            </div> <!-- /.grid-item -->

        </div> <!-- /.project-gallery -->
    </div> <!-- /.container -->
</div> <!-- /#project-section -->
@include('partials.banner')
@endif