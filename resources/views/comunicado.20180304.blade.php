<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- For IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- For Resposive Device -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Comunicados - Clube de Desbravadores Pioneiros da Colina</title>

    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="56x56" href="{{ asset('images/fav-icon/icon.png') }}">


    <!-- Main style sheet -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    <!-- responsive style sheet -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}">


    <!-- Fix Internet Explorer ______________________________________-->

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="{{ asset('vendor/html5shiv.js') }}"></script>
    <script src="{{ asset('vendor/respond.js') }}"></script>
    <![endif]-->

</head>

<body>
<div class="main-page-wrapper">
    @include('partials.menu')

    <!--
    =====================================================
        Theme Inner page Banner
    =====================================================
    -->
    <section class="inner-page-banner">
        <div class="opacity">
            <div class="container">
                <!-- <h2>Blog</h2> -->
                <ul>
                    <li><a href="/">Home</a></li>
                    <li>/</li>
                    <li>Comunicados</li>
                </ul>
            </div> <!-- /.container -->
        </div> <!-- /.opacity -->
    </section> <!-- /.inner-page-banner -->



    <!--
    =====================================================
        Blog Page Details
    =====================================================
    -->
    <article class="blog-details-page">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-fix">
                <div class="blog-details-post-wrapper">
                    <div class="post-heading">
                        <h4>São Paulo, 04 de março de 2018.</h4>
                    </div> <!-- /.post-heading -->
                    <h6>Senhores Pais,<br />Atenção para alguns recados importantes:</h6>
                    <aside style="float: right; margin-left: 3px">
                        <img src="{{ asset('images/calendarioMarco2018.png') }}">
                    </aside>
                    <p> <strong>FOLGA – 11/03</strong><br />
                        Neste próximo domingo dia 11/03, não teremos reunião, retornaremos as atividades no dia 18/03.
                    </p>
                    <p> <strong>HORÁRIO DAS REUNIÕES</strong><br />
                        O horário de reunião é das 08h30 às 12h, sendo o horário de atendimento da secretaria e tesouraria é das 09h às 11h.
                    </p>
                    <p> <strong>
                            CAMPORI – DSA
                        </strong><br />
                        O tão sonhado Campori da Divisão Sul Americana está a cada dia mais próximo, garanta sua VAGA conversando com o setor financeiro! Planeje-se! Você não pode ficar fora desse evento, lembrando que as inscrições já são em no final de MARÇO.
                    </p>
                    <p> <strong>
                            PROJETO MISSIONÁRIO
                        </strong><br />
                        No dia 24/03 teremos nossa primeira saída do ano. Serão 4 saídas, sendo divididos em 4 grupos. Mais informações nos próximos comunicados.
                    </p>
                    <p> <strong>
                            DOCUMENTAÇÃO
                        </strong><br />
                        É de extrema importância que todos os desbravadores estejam com sua documentação em dia.<br />
                        Segue lista de documentos:
                    <dl>
                        <li>Ficha de inscrição 2018 - ATUALIZADA</li>
                        <li>Ficha médica 2018 - ATUALIZADA</li>
                        <li>Foto Cópia – RG do Desbravador</li>
                        <li>Foto Cópia – Carteirinha do SUS do desbravador</li>
                        <li>Foto Cópia – RG e CPF do Responsável</li>
                        <li>Foto Cópia – Carteirinha de vacinação (COM VACINA DA FEBRE AMARELA)</li>
                        <li>Foto Cópia – Carteirinha do convênio (se tiver)</li>
                    </dl>
                    </p>
                    <p> <strong>
                            DEZ DIAS DE ORAÇÃO
                        </strong><br />
                        Agradecemos a todos que acompanharam os dez dias de oração em nosso site. Deus esteve presente neste projeto!
                    </p>
                    <p> <strong>
                            MENSALIDADES
                        </strong><br />
                        Para ajudar e facilitar para todas nossas mensalidades alteraram de valor passando a custar R$20,00.
                        Converse com a tesouraria do clube.
                    </p>
                    <p> <strong>CONTAS PARA DEPÓSITO E TRANSFERÊNCIA - Igreja Unasp - União Central Brasileira</strong><br />
                        <dl>
                            <li>Bradesco: Ag: 3396-0, C/c: 2226-8</li>
                            <li>Banco do Brasil: Ag: 1740-X. C/c: 18.823-9</li>
                            <li>Banco Santander: Ag: 3719, C/c: 13000048-0</li>
                            <li>Banco Itaú: Ag: 1661, C/c: 11480-9</li>
                        </dl>
                    <blockquote>
                        <strong>Enviar comprovante até sexta feira às 12h (meio-dia) da SEMANA DA OPERAÇÃO para
                            <a href="mailto:tesouraria@pioneirosdacolina.com.br">tesouraria@pioneirosdacolina.com.br</a>
                            e acrescentar as seguintes informações:</strong> <br />
                        Nome do desbravador / Unidade / Telefone para Contato / Valor Referente a: (Ex: Doação, Caminhada, mensalidade, etc.)
                    </blockquote>
                    <p><strong>Em caso de dúvidas entrar em contato com o tesoureiro Jordi Bitencourt (11) 980.555.921</strong></p>
                    <h6><u>COMPROVANTES NÃO DEVEM MAIS SER ENCAMINHADOS POR WHATSAPP, SOMENTE POR EMAIL.</u></h6>

                    </p>
                    <p> <strong>CONTATOS DA DIRETORIA</strong><br />
                        <ul>
                        <li><strong>Diretor</strong>  – Marcos Nunes: (11) 985.399.309 (TIM)</li>
                        <li><strong>Secretária</strong>  – Bianca Vargas: (11) 952.323.983 (TIM)</li>
                        <li><strong>Secretária </strong> – Ingrid Barbosa: (11) 953.600.619 (TIM)</li>
                        <li><strong>Tesoureiro</strong>  – Vinicius Freitas: (11) 970.474.948 (TIM)</li>
                        <li><strong>Tesoureiro </strong> – Jordi Bitencourt: (11) 980.555.921 (VIVO)</li>
                        <li><strong>Diretora Associada</strong>  – Kelly: (11) 987.688.581 (TIM)</li>
                        <li><strong>Diretora Associada</strong>  – Caroline Deó: (11) 977.009.423 (TIM)</li>
                        <li><strong>Diretor Associado</strong>  – Eduardo: (11) 976.126.686 (VIVO)</li>
                        <li><strong>Diretor Associado</strong>  – Kevin: (11) 991.991.783 (VIVO)</li>
                        <li><strong>Ancião</strong>  – Alair: (11) 997.063.957 (VIVO)</li>
                        <li><strong>Ancião</strong>  – Eber: (11) 996.435.721 (VIVO)</li>
                        <li><strong>Conselheiro Geral</strong>  – Vigani: (11) 981.151.236 (TIM)</li>
                        <li><strong>Diretor de Patrimônio</strong>  – Enéas Oliveira: (11) 947.781.888 (TIM)</li>
                        <li><strong>Diretor de TI</strong>  – Mayck Cipriano: (11) 989.526.529 (CLARO)</li>
                        </ul>
                    </p>
                    <p class="text-right">
                        Atenciosamente,<br />
                        A Direção
                    </p>
                </div> <!-- /.blog-details-post-wrapper -->
            </div> <!-- /.col- -->
        </div> <!-- /.container -->
    </article>


        @include('partials.footer')
    </div> <!-- /.main-page-wrapper -->
</body>
</html>