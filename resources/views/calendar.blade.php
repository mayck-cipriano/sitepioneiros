<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- For IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- For Resposive Device -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Calendário - Clube de Desbravadores Pioneiros da Colina</title>

    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="56x56" href="{{ asset('images/fav-icon/icon.png') }}">


    <!-- Main style sheet -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    <!-- responsive style sheet -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}">


    <!-- Fix Internet Explorer ______________________________________-->

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="{{ asset('vendor/html5shiv.js') }}"></script>
    <script src="{{ asset('vendor/respond.js') }}"></script>
    <![endif]-->

</head>

<body>
<div class="main-page-wrapper">
    @include('partials.menu')

    <!--
    =====================================================
        Theme Inner page Banner
    =====================================================
    -->
    <section class="inner-page-banner">
        <div class="opacity">
            <div class="container">
                <!-- <h2>Blog</h2> -->
                <ul>
                    <li><a href="/">Home</a></li>
                    <li>/</li>
                    <li>Calendário</li>
                </ul>
            </div> <!-- /.container -->
        </div> <!-- /.opacity -->
    </section> <!-- /.inner-page-banner -->



    <!--
    =====================================================
        Blog Page Details
    =====================================================
    -->
    <article class="blog-details-page">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-fix">
                <div class="blog-details-post-wrapper">
                    <div class="post-heading">
                        <h4>ACOMPANHE NOSSOS EVENTOS DURANTE O ANO!!!</h4>
                    </div> <!-- /.post-heading -->
                    <iframe src="https://calendar.google.com/calendar/embed?src=pioneirosdacolinaunaspsp%40gmail.com&ctz=America%2FSao_Paulo" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
                </div> <!-- /.blog-details-post-wrapper -->
            </div> <!-- /.col- -->
        </div> <!-- /.container -->
    </article>


        @include('partials.footer')
    </div> <!-- /.main-page-wrapper -->
</body>
</html>